;(function () {
"use strict";

var canvas = document.getElementById("game");
canvas.width = window.innerWidth;
canvas.height = window.innerHeight;
var ctx = canvas.getContext("2d");
var canvasW = canvas.width;
var canvasH = canvas.height;
var enemies = [];
var keys = [];
var newLives = [];
var livesInterval;
var heroesBullets = [];
var enemiesBullets = [];
var currentLivetype;
var currentLivetypeTitle;
var enemiesInterval;
var game = {
  isPlay: false,
  createLivesTime: 10000,
  isShoot: true,
  newLives: 1,
  speed: 2,
  level: 1,
  maxLevels: 3,
  levelDuration: 1000,
  result: 0,
  heroBulletsCount: 10,
  heroHitCount: 0,
  enemyHitCount: 0,
  maxEnemies: 3,
  bulletPower: 0,
  health: 100,
  maxKick: 5,
  startEnemiesPos: 40,
  enemiesPosSpace: 66,
  messages: {
    victory: 'Victory',
    defeat: 'Game over'
  },
  controls: {
    play: 'play',
    stop: 'stop',
    continue: 'continue',
    restart: 'restart'
  },
  typesLive: [
    {
      title: 'heart',
      img: 'assets/images/heart.png',
    },
    {
      title: 'water',
      img: 'assets/images/water.png',
    },
  ],
  worldBgs: {
    'firstbg': 'assets/images/firstbg.jpg',
    'secondbg': 'assets/images/secondbg.jpg',
    'thirdbg': 'assets/images/thirdbg.jpg',
  },
  healthBar: {
    width: 60,
    height: 5,
    color: 'green'
  },
  appereanceEnemy: [
    {
      img: 'assets/images/enemy.png',
      imgCollapsed: 'assets/images/enemycollapsed.png',
    },
    {
      img: 'assets/images/enemyl2.png',
      imgCollapsed: 'assets/images/enemycollapsedl2.png',
    },
    {
      img: 'assets/images/enemyl3.png',
      imgCollapsed: 'assets/images/enemycollapsedl3.png',
    },
  ],
};
var introWrap = document.getElementById('intro-wrap');
var faqBlock = document.getElementById('faq');
var mobileCtrls = document.getElementById('mobile-ctrls');
var resultBox = document.getElementById('result-box');
/**
 * Games loop and funcs
 * gameLoop() -> Start all funcs for draw and update objects
 * startGame() -> Start game
 * stopGame()() -> Stop game
 */
  function startGame() {
    sound.gameSoundPlay();
    faqBlock.classList = '';
    mobileCtrls.classList.add('show');
    world._init();
    gameLoop();
  }

  function gameLoop() {
    if(game.isPlay){
      requestAnimationFrame(gameLoop);
      world.redraw();
    }
  }
/**
*Heroes controls
*/
  document.body.addEventListener("keydown", function(e) {
     keys[e.keyCode] = true;
  });

  document.body.addEventListener("keyup", function(e) {
    keys[e.keyCode] = false;
  });

/**
*Games controls
*/
  document.getElementById(game.controls.play).onclick = function() {
    startGame();
    sound.introMusic.pause();
  };

  var mobileSpit = false;
  document.getElementById('mob-ctrl-spit').onclick = function() {
    mobileSpit = true;
    if(mobileSpit) {
      console.log('spittttt')
    }
  };

  document.getElementById('next-level-btn').onclick = function() {
    sound.gameSoundPlay();
    sound.levelMusic.pause();
    resultBox.classList = '';
    gameObj.continuePlay();
    this.classList = '';
    mobileCtrls.classList.add('show');
    gameLoop();
  };

  document.getElementById(game.controls.restart).onclick = function() {
    gameObj.reset();
    startGame();
    resultBox.classList = '';
    this.classList = '';
    mobileCtrls.classList.add('show');
    // location.reload();
    // startGame();
  };

/**
*World
*Functions draw and update games world
*/
var World = function() {
  this.redraw = function() {
    changeBg(game.level);
    mainHero.redraw();
    gameObj.update();
    redrawEnemies();
  }

  this._init = function() {
    setInterval(scene.drawRain, 50);
    game.isPlay = true;
    game.heroBulletsCount = 10,
    game.heroHitCount = 0,
    game.enemyHitCount = 0,
    createEnemies();
    introWrap.classList.add('hide');
    // startCreateEnemies();
  }

  function changeBg(level) {
    var bg;

    if(level >= 1 && level <= 2) {
      bg = game.worldBgs.firstbg;
    }
    else if(level >= 3 && level <= 4) {
      bg = game.worldBgs.secondbg;
    }
    else if(level >= 5) {
      bg = game.worldBgs.thirdbg;
    }

    scene.drawBg(bg);
  }
}
var world = new World();

/**
*Game
*Render html elements(popups, panels, buttons),
*General games functions(reset, victory, defeat)
*Functions for generate html elements and sectios
*/
var  Game = function() {
  var self = this;
  var panel = document.getElementById("panel");
  var level  = document.getElementById("level");
  var bullets = document.getElementById('bullets');
  var resultMessage = document.getElementById('result-message');
  var nextLevelBtn = document.getElementById('next-level-btn');
  var restartBtn = document.getElementById('restart');
  var faqBlock = document.getElementById('faq');
  var options = {
    heroX: canvasW,
    period: 4,
    speeding: 0.5,
    startResult: 0,
    level: 1,
    lives: 3,
    speed: 2,
    enemies: 0,
    newLives: 0,
    levelMessage: 'WINNER',
    victoryMessage: 'VICTORY',
    lostMessage: 'LOST',
  };
  game.result = (game.speed/options.period).toFixed(0);

  this.update = function() {
    renderPanel();
    changeLevel();
    defeat();
    //victory();
    start();
  }

  this.continuePlay = function() {
    game.isPlay = true;
  }

  this.reset = function() {
    mainHero.x = options.heroX;
    game.result = options.startResult;
    game.level = options.level;
    game.speed = options.speed;
    enemies.length = options.enemies;
    newLives.length = options.newLives;
    enemiesBullets = [];
    heroesBullets = [];
  }

   this.healthBar = function(hit, x, y) {
    ctx.beginPath();
    ctx.fillStyle = game.healthBar.color
    ctx.fillRect(x, y, game.healthBar.width * (((game.maxKick - hit) * 100) / game.maxKick)/100, game.healthBar.height);
    ctx.stroke();
  }

  function renderPanel() {
    result.innerHTML = "Результат " +  game.result++;
    level.innerHTML = "Рівень " +  game.level;
    bullets.innerHTML = "Плювки " +  game.heroBulletsCount;
    checkIsBullets();
  }

  function stop() {
    game.isPlay = false;
  }

  function resetData() {
    game.enemyHitCount = 0;
    game.heroHitCount = 0;
    game.heroBulletsCount = 10;
    heroesBullets = [];
    enemiesBullets = [];
    faqBlock.classList = '';
    mobileCtrls.classList = '';
    stop();
  }

  function changeLevel() {
    if (enemies.length === 0) {
      sound.gameMusic.pause();
      sound.levelSoundPlay();
      game.speed = game.speed - options.speeding;
      game.level++;
      showSplash(game.level);
      resultMessage.innerHTML = options.levelMessage;
      restartBtn.classList = '';
      nextLevelBtn.classList.add('show');
      mainHero.x = options.heroX;
      createEnemies();
      resetData();
    }
  }


  function checkIsBullets() {
    if(game.heroBulletsCount === 0) {
      bullets.className = "no-bullets ";
    }
    else {
      bullets.className = "";
    }
  }

  function toggleElem(thisElem, elem) {
    thisElem.classList.remove("hide");
    elem.classList.remove("hide");
    thisElem.classList.add("hide");
    elem.classList.add("show");
  }

  function showSplash(class_) {
    resultBox.classList = '';
    resultBox.classList.add('level-' + class_);
    resultBox.classList.add('show');
  }

  function defeat(){
    if (game.heroHitCount === game.maxKick) {
      resetData();
      showSplash();
      game.level = 1;
      nextLevelBtn.classList = '';
      restartBtn.classList.add('show');
      resultMessage.innerHTML = options.lostMessage;
    }
  }

  function victory() {
    if (game.enemyHitCount === game.maxKick) {
      game.isPlay = false;
    }
  }

  function start() {
    panel.classList.add('show');
  }
}
var gameObj = new Game();

var Intro = function() {
  var introWrap = document.getElementById('stories');
  var playBtn = document.getElementById('play');
  var storyLength = introWrap.childElementCount;
  var currentStory = -1;
  var storyInterval;
  var storyIntervalTime = 1000;

  function setStoryActive() {
    storyInterval = setInterval(function(){
      if(currentStory < storyLength) {
        currentStory = currentStory <= storyLength-1 ? ++currentStory : 0;
        isWatchedStory(currentStory, storyLength);
        removeStoryActive();
        introWrap.children[currentStory].classList.add('active');
      }
      else {
        clearInterval(storyInterval);
        if(!game.isPlay) {
          faqBlock.classList.add('show');
        }
      }
    }, storyIntervalTime);
  }

  function removeStoryActive() {
    for(var i = 0; i < storyLength; i++) {
      introWrap.children[i].classList.remove("active");
    }
  }

  function isWatchedStory(id, length) {
    var isWatchedStory = localStorage.getItem('watchedStory');
    if(isWatchedStory) {
      playBtn.classList.add('show');
    }
    else {
      if(id === length) {
        localStorage.setItem('watchedStory', true);
        playBtn.classList.add('show');
      }
    }
  }

  setStoryActive();
  isWatchedStory(currentStory, storyLength);
}

var intro = new Intro();

/**
*Create Scenery object with bg and clouds
*Functions for move bg, move clouds and clear canvas
*/
var Scenery = function() {
  var self = this;
  var canvasR = document.getElementById("rain");
  var ctxR = canvasR.getContext("2d");
  var options = {
    bgStartX : 0,
    bgEndX: canvasW,
    speedBg: 2/game.speed,
    speedClouds: 8/game.speed,
    bgOffsetX: 10,
    rainColor: 'rgba(255,255,255,0.5)',
    rainWidth: 1,
    rainType: 'round'
  }
  var background = new Image;
  var init = [];
  var maxParts = 1000;
  var particles = [];

  this.drawBg = function(bg) {
    background.src = bg;
    ctx.drawImage(background, options.bgStartX , 0, canvasW , canvasH);
  }
  canvasR.width = window.innerWidth;
  canvasR.height = window.innerHeight;
  ctxR.strokeStyle = options.rainColor;
  ctxR.lineWidth = options.rainWidth;
  ctxR.lineCap = options.rainType;

  this.drawRain = function() {
    ctxR.clearRect(0, 0, canvasW, canvasH);
    for(var c = 0; c < particles.length; c++) {
      var p = particles[c];
      ctxR.beginPath();
      ctxR.moveTo(p.x, p.y);
      ctxR.lineTo(p.x + p.l * p.xs, p.y + p.l * p.ys);
      ctxR.stroke();
    }
    moveRain();
  }

  function generateRain() {
    for(var a = 0; a < maxParts; a++) {
      init.push({
        x: Math.random() * canvasW,
        y: Math.random() * canvasH,
        l: Math.random() * 2,
        xs: -8 + Math.random() * 3 + 9,
        ys: Math.random() * 10 + 30
      })
    }

    for(var b = 0; b < maxParts; b++) {
      particles[b] = init[b];
    }
  }
  generateRain();

  function moveRain() {
    for(var b = 0; b < particles.length; b++) {
      var p = particles[b];
      p.x += p.xs;
      p.y += p.ys;
      if(p.x > canvasW || p.y > canvasH) {
        p.x = Math.random() * canvasW;
        p.y = -20;
      }
    }
  }

}
var scene = new Scenery();


/**Sounds class*/
var Sound = function() {
  var sounds = {
    intro: {
      file: 'assets/audio/intro.wav',
      isLoop: true,
      volume: 0
    },
    game: {
      file: 'assets/audio/intro.wav',
      isLoop: true,
      volume: 0
    },
    level: {
      file: 'assets/audio/intro.wav',
      isLoop: true,
      volume: 0
    },
    spit: {
      file: 'assets/audio/shoot.wav',
      volume: 0
    }
  }

  this.introMusic = new Audio(sounds.intro.file);
  this.gameMusic = new Audio(sounds.game.file);
  this.levelMusic = new Audio(sounds.level.file);
  this.spitSound = new Audio(sounds.spit.file);
  this.spitSoundEnem = new Audio(sounds.spit.file);

  this.introSoundPlay = function() {
    this.introMusic.loop = sounds.intro.isLoop;
    this.introMusic.volume = sounds.intro.volume;
    this.introMusic.play();
  }

  this.gameSoundPlay = function() {
    this.gameMusic.currentTime = 0;
    this.gameMusic.loop = sounds.game.isLoop;
    this.gameMusic.volume = sounds.game.volume;
    this.gameMusic.play();
  }

  this.levelSoundPlay = function() {
    this.levelMusic.currentTime = 0;
    this.levelMusic.loop = sounds.level.isLoop;
    this.levelMusic.volume = sounds.level.volume;
    this.levelMusic.play();
  }

  this.spitSoundPlay = function() {
    this.spitSound.volume = sounds.spit.volume;
    this.spitSound.play();
  }

  this.spitSoundEnemPlay = function() {
    this.spitSoundEnem.volume = sounds.spit.volume;
    this.spitSound.play();
  }
}

var sound = new Sound();
sound.introSoundPlay();

/**Shoot class
*Functions update() and draw() for draw, update bullets position*/
var Bullet = function() {
  var currentBulletY;
  this.bullet = {
    height: 50,
    width: 130,
    shooter: '',
    rotateSpeed: 3,
    rotateSpeedStep: 2
  };

  this.redraw = function(img, x, y) {
    ctx.drawImage(img, x, y, this.bullet.width, this.bullet.height);
    this.bullet.rotateSpeed += this.bullet.rotateSpeedStep;
    //drawRotatedImage(this.bulletImg, x, y, this.bullet.rotateSpeed);
    removeBullet.call(this);
  }

  this.canShoot = function(frequency) {
    setTimeout(function() {
      game.isShoot = true;
    }, frequency);
  }

  function drawRotatedImage(image, x, y, angle) {
    ctx.save();
    ctx.translate(x, y);
    ctx.rotate(angle * Math.PI/180);
    ctx.drawImage(image, 0, -150, 30, 30);
    ctx.restore();
  }

  function removeBullet() {
    for (var i = 0; i < heroesBullets.length; i++) {
      if(heroesBullets[i].bulletPosX < 0
        || heroesBullets[i].bulletPosY + 120 > canvasH
        || heroesBullets[i].bulletPosY < 0) {
        heroesBullets.splice(heroesBullets.indexOf(this), 1);
      }
    }
    for (var i = 0; i < enemiesBullets.length; i++) {
      if(enemiesBullets[i].bulletPosX > canvasW) {
        enemiesBullets.splice(enemiesBullets.indexOf(this), 1);
      }
    }
  }
}

/**Create new hero obj
*Functions update() and draw() for draw, update position
*Functions run(), back(), crouch(), jump(), changePos(), checkPosY(), checkPosX for change position
*Function heroCollapsed() for collapsed with enemy
*Function addLive() for add new live
*Function jumpOnPlatform() for jump hero on platform
*/
var Hero = function() {
  var hero = {
    height: 331,
    width: 117,
    x: canvasW,
    y: canvasH,
    bulletImgL: 'assets/images/bulletl.png',
    speed: 10/game.speed,
    dx: 0,
    dy: 0,
    stepX: 5,
    fullH: 325,
    crouchH: 296,
    offsetY: 100,
    jumping: false,
    crouch: false,
    canCrouch: true,
    moving: false,
    friction: 0.9,
    gravity: 1,
    bulletFrequency: 1000,
    bulletsAddFrequency: 5000,
    dxBullet: 5,
    collapseTime: 1500,
    appereance: [
      {
        img: 'assets/images/hero.png',
        imgCollapsed: 'assets/images/active.png',
        imgCrouch: 'assets/images/crouchl1.png',
        imgCollapsedCrouch: 'assets/images/crouch_collapsedl1.png',
      },
      {
        img: 'assets/images/herol2.png',
        imgCollapsed: 'assets/images/herol2active.png',
        imgCrouch: 'assets/images/crouchl1.png',
        imgCollapsedCrouch: 'assets/images/crouch_collapsedl1.png',
      },
      {
        img: 'assets/images/herol3.png',
        imgCollapsed: 'assets/images/herol3active.png',
        imgCrouch: 'assets/images/crouchl1.png',
        imgCollapsedCrouch: 'assets/images/crouch_collapsedl1.png',
      },
    ],
  };
  var controls = {
    run: '39',
    crouch: '40',
    back: '37',
    jump: '38',
    spit: '32'
  };
  var collapsed;
  var heroImg = new Image();
  var imgCollapsed = new Image();
  var bulletImg = new Image();
  var angleBullet = 0;

  bulletImg.src = hero.bulletImgL;
  this.x = hero.x;

  this.redraw = function() {
    draw.call(this);
    update.call(this);
  }

  function draw() {
    if(!collapsed) {
      ctx.drawImage(heroImg, this.x, hero.y - hero.offsetY, hero.width , hero.height);
    }
    else {
      ctx.drawImage(imgCollapsed, this.x, hero.y - hero.offsetY, hero.width , hero.height);
    }

  }
  function update() {
    var angleCoef = 0;
    changeLook();
    heroHit.call(this);
    gameObj.healthBar(
      game.heroHitCount,
      this.x + hero.width/2 - game.healthBar.width/2,
      hero.y - 120,
    );

    for (var i = 0; i < heroesBullets.length; i++) {
      if(heroesBullets[i].power > 120) {
        angleCoef = 100;
        angleBullet -= heroesBullets[i].power/angleCoef;
      }
      else if(heroesBullets[i].power > 70 && heroesBullets[i].power < 120) {
        angleCoef = 150;
        angleBullet -= heroesBullets[i].power/angleCoef;
      }
      else {
        angleCoef = 18;
        angleBullet += heroesBullets[i].power/angleCoef;
      }

      heroesBullets[i].bulletPosX -= 10;
      heroesBullets[i].bulletPosY += (heroesBullets[i].bulletPosY * Math.sin(angleBullet * Math.PI / 180))/90;
      heroesBullets[i].redraw(bulletImg, heroesBullets[i].bulletPosX, heroesBullets[i].bulletPosY);
    }

    //Direction -> jump
    if (keys[controls.jump]) {
      jump.call(this);
    }
    //Direction -> crouch
    if (keys[controls.crouch]) {
      if (hero.canCrouch) {
        crouch.call(this);
      }
    }
    //Direction -> spit
    if (keys[controls.spit] || mobileSpit) {
      spit.call(this);
      mobileSpit = false;
      if(game.heroBulletsCount > 0) {
        sound.spitSoundPlay();
      }
    }
    //Direction -> stand up
    if (!keys[controls.crouch]) {
      standUp.call(this);
    }
    //Direction -> run
    if (keys[controls.run]) {
      run.call(this);
    }
    //Direction -> back
    if (keys[controls.back]) {
      back.call(this);
    }

    changePos.call(this);
    checkPosX.call(this);
    checkPosY.call(this);
  }

  function run(){
    if (hero.dx < hero.speed) {
      hero.dx += hero.stepX;
    }
  }

  function crouch() {
    if (!hero.crouch){
      hero.crouch = true;
      hero.height = hero.crouchH;
    }
  }

  function back(){
    if (hero.dx > -hero.speed) {
        hero.dx -= hero.stepX;
    }
  }

  function jump() {
    if (!hero.jumping) {
     hero.jumping = true;
     hero.dy = -hero.speed * 3;
    }
  }

  function spit() {
    if(game.isShoot && game.heroBulletsCount > 0 && !hero.crouch) {
      var bullet= new Bullet();
      angleBullet = 0;
      game.heroBulletsCount--;
      game.isShoot = false;
      bullet.bulletPosX = this.x - hero.width/2;
      bullet.bulletPosY = hero.y - hero.height/2 + 50;
      bullet.power = getSpitPower();
      bullet.shooter = 'hero';
      heroesBullets.push(bullet);
      bullet.canShoot(hero.bulletFrequency);
    }
  }

  function getSpitPower() {
    var power = document.querySelector('.panel .power .power-items');
    return power.clientWidth;

  }

  function changePos() {
    hero.dx *= hero.friction;
    hero.dy += hero.gravity;

    this.x += hero.dx;
    hero.y += hero.dy;
  }

  function checkPosX() {
    if (this.x >= canvasW - hero.width) {
        this.x = canvasW - hero.width;    }
    else if (this.x <= canvasW/2) {
      this.x = canvasW/2;
      this.x += hero.dx;
      hero.y += hero.dy;
    }
  }

  function checkPosY() {
    if (hero.y >= canvasH - hero.height) {
        hero.y = canvasH - hero.height;
        hero.jumping = false;
    }
  }

  function standUp() {
    if (hero.crouch) {
      hero.crouch = false;
      hero.height = hero.fullH;
    }
  }

  function changeLook() {
    if(game.level <= game.maxLevels) {
      for(var i = 0; i <= hero.appereance.length; i++) {
        if(i + 1 === game.level) {
          if(!hero.crouch) {
            heroImg.src = hero.appereance[i].img;
            imgCollapsed.src = hero.appereance[i].imgCollapsed;
          }
          else {
            heroImg.src = hero.appereance[i].imgCrouch;
            imgCollapsed.src = hero.appereance[i].imgCollapsedCrouch;
          }
        }
      }
    }
    else {
      if(!hero.crouch) {
        heroImg.src = hero.appereance[hero.appereance.length - 1].img;
        imgCollapsed.src = hero.appereance[hero.appereance.length - 1].imgCollapsed;
      }
      else {
        heroImg.src = hero.appereance[hero.appereance.length - 1].imgCrouch;
        imgCollapsed.src = hero.appereance[hero.appereance.length - 1].imgCollapsedCrouch;
      }
    }
  }



  function heroHit() {
    for (var i = 0; i < enemiesBullets.length; i++) {
      if ((this.x < enemiesBullets[i].bulletPosX
         && this.x + hero.width - 50 > enemiesBullets[i].bulletPosX)
         && hero.y - hero.height/2  < enemiesBullets[i].bulletPosY ) {
        collapsed = true;
        enemiesBullets.splice(enemiesBullets[i], 1);
        game.heroHitCount++;
        setTimeout(function() {
          collapsed = false;
        }, hero.collapseTime);
      }
    }
  }

  function addBullets() {
    game.heroBulletsCount++;
  }

  function initHero() {
    setInterval(function(){
      addBullets();
    }, hero.bulletsAddFrequency)
  }

  initHero();
}
var mainHero = new Hero();


/**Enemy gypsy object*/
var Gypsy = function(x, img, imgCollapsed) {
  var gypsy = {
    x: 40,
    y: canvasH - 80,
    width: 132,
    height: 345,
    bulletImgR: 'assets/images/bullet.png',
    bulletFrequency: 4000,
    dxBullet: 5,
    dx: 1.15,
    collapseTime: 1500
  };

  var bulletImg = new Image();
  var gypsyImg = new Image();
  var gypsyImgCollapsed = new Image();

  gypsyImg.src = img;
  gypsyImgCollapsed.src = imgCollapsed;
  this.x = x;
  this.y = gypsy.y;
  bulletImg.src = gypsy.bulletImgR;
  this.gypsyWidth = gypsy.gypsy;
  this.collapsed = false;
  this.hitCount = 0;
  this.canShoot = false;
  this.isMiddlePos = false;
  this.draw = function() {
    if(!this.collapsed) {
      ctx.drawImage(gypsyImg, this.x, this.y - gypsy.height, gypsy.width , gypsy.height);
    }
    else {
      ctx.drawImage(gypsyImgCollapsed, this.x, this.y - gypsy.height, gypsy.width , gypsy.height);
    }
  }
  this.update = function() {
    //changeLook.call(this);
    //enemyHit.call(this);
    //this.x += 1;
    moveEnemy.call(this);
    //removeEnemy.call(this);
  }


  this.bulletsDraw = function() {
    for (var i = 0; i < enemiesBullets.length; i++) {
      enemiesBullets[i].bulletPosX += gypsy.dxBullet;
      enemiesBullets[i].bulletPosY -= .3;
      enemiesBullets[i].redraw(bulletImg, enemiesBullets[i].bulletPosX, enemiesBullets[i].bulletPosY);
    }
    gypsyHit.call(this);
  }

  function moveEnemy() {
    var dx = game.level <= game.maxLevels ? gypsy.dx * game.level : gypsy.dx * game.maxLevels;
    if(!this.isMiddlePos) {
      this.x += dx;
      if(this.x > canvasW/2 - gypsy.width) {
        this.isMiddlePos = true;
      }
    }
    else {
      this.x -= dx;
      if(this.x < game.startEnemiesPos) {
        this.isMiddlePos = false;
      }
    }
  }

  function gypsyHit() {
    for(var j = 0; j < enemies.length; j++) {
      gameObj.healthBar(
         enemies[j].hitCount,
         enemies[j].x + gypsy.width/2 - game.healthBar.width/3,
         enemies[j].y - gypsy.height - 20,
         );

      for (var i = 0; i < heroesBullets.length; i++) {
        if (enemies[j].x + gypsy.width > heroesBullets[i].bulletPosX
             && enemies[j].x < heroesBullets[i].bulletPosX
             && (enemies[j].y - gypsy.height - 70 < heroesBullets[i].bulletPosY
             && enemies[j].y > heroesBullets[i].bulletPosY)
          ) {

          game.enemyHitCount++;
          heroesBullets.splice(heroesBullets[i], 1);
          this.collapsed = true;
          this.hitCount++;
          removeEnemy.call(this, enemies, enemies[j]);
          setTimeout(function() {
            removeCollapsedStatus(enemies);
          }, gypsy.collapseTime);
        }
      }
    }
  }

  function removeCollapsedStatus(enemies) {
    for(var i = 0; i < enemies.length; i++) {
      enemies[i].collapsed = false;
    }
  }

  function removeEnemy(enemies, enemy) {
    if(this.hitCount >= game.maxKick) {
      enemies.splice(enemy, 1);
    }
  }

  function spit() {
    setTimeout(function(){
      var bullet= new Bullet();
      bullet.bulletPosX = enemies[enemies.length - 1].x + gypsy.width/2;
      bullet.bulletPosY = enemies[enemies.length - 1].y - gypsy.height;
      bullet.shooter = 'gypsy';
      enemiesBullets.push(bullet);
      //sound.spitSoundEnemPlay();
      spit();
    }, Math.floor((Math.random() * gypsy.bulletFrequency) + 2000 * game.level))
  }

  //spit();
}

function createEnemies() {
  var enemy;
  var x;
  var enemiesPerLevel = game.level <= game.maxLevels ? game.level : game.maxEnemies;
  for (var i = 0; i < enemiesPerLevel; i++) {
    x = (game.startEnemiesPos + enemiesPerLevel * game.enemiesPosSpace) - i * game.enemiesPosSpace;
    enemy = new Gypsy(x, game.appereanceEnemy[i].img, game.appereanceEnemy[i].imgCollapsed);
    enemies.unshift(enemy);
  }
}

function redrawEnemies() {
  for (var i = 0; i < enemies.length; i++) {
    enemies[i].update();
    enemies[i].draw();
    enemies[i].bulletsDraw();
  }
}
  /**
  * General functions for canvas animate, clear intervals and random
  *
  */
    function intervalClear(interval) {
      clearInterval(interval);
    }

    function clearCanvas() {
      ctx.clearRect(0, 0, canvasW, canvasH);
    }

    function randomVal(periond, minStep) {
      return Math.floor(Math.random() *  periond) + minStep;
    }
  /**
  *Canvas animate
  */
  if(game.isPlay) {
    (function() {
      var lastTime = 0;
      var vendors = ['ms', 'moz', 'webkit', 'o'];
      for (var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
          window.requestAnimationFrame = window[vendors[x]+'RequestAnimationFrame'];
          window.cancelAnimationFrame = window[vendors[x]+'CancelAnimationFrame']
                                     || window[vendors[x]+'CancelRequestAnimationFrame'];
      }

      if (!window.requestAnimationFrame)
          window.requestAnimationFrame = function(callback, element) {
            var currTime = new Date().getTime();
            var timeToCall = Math.max(0, 16 - (currTime - lastTime));
            var id = window.setTimeout(function() { callback(currTime + timeToCall); },
              timeToCall);
            lastTime = currTime + timeToCall;
            return id;
          };

      if (!window.cancelAnimationFrame)
          window.cancelAnimationFrame = function(id) {
              clearTimeout(id);
          };
    }());
  }
})();
